#include "adw-demo-page-tab-view.h"

#include <glib/gi18n.h>

#include "adw-tab-view-demo-window.h"

struct _AdwDemoPageTabView
{
  AdwBin parent_instance;

  char *title;
  GIcon *icon;
};

G_DEFINE_FINAL_TYPE (AdwDemoPageTabView, adw_demo_page_tab_view, ADW_TYPE_BIN)

enum {
  PROP_0,
  PROP_TITLE,
  PROP_ICON,
  LAST_PROP,
};

static GParamSpec *props[LAST_PROP];

char **icon_names2 = NULL;
gsize c_icon_names = 0;

static void
init_icon_names (GtkIconTheme *theme)
{
  if (icon_names2)
    return;

  icon_names2 = gtk_icon_theme_get_icon_names (theme);
  c_icon_names = g_strv_length (icon_names2);
}

static GIcon *
get_random_icon (void)
{
  GdkDisplay *display = gdk_display_get_default ();
  GtkIconTheme *theme = gtk_icon_theme_get_for_display (display);
  int index;

  init_icon_names (theme);

  index = g_random_int_range (0, c_icon_names);

  return g_themed_icon_new (icon_names2[index]);
}

static void
demo_run_cb (AdwDemoPageTabView *self)
{
  AdwTabViewDemoWindow *window = adw_tab_view_demo_window_new ();
  GtkRoot *root = gtk_widget_get_root (GTK_WIDGET (self));

  adw_tab_view_demo_window_prepopulate (window);

  gtk_window_set_transient_for (GTK_WINDOW (window), GTK_WINDOW (root));
  gtk_window_present (GTK_WINDOW (window));
}

static void
adw_demo_page_tab_view_finalize (GObject *object)
{
  AdwDemoPageTabView *self = ADW_DEMO_PAGE_TAB_VIEW (object);

  g_clear_pointer (&self->title, g_free);
  g_clear_object (&self->icon);

  G_OBJECT_CLASS (adw_demo_page_tab_view_parent_class)->finalize (object);
}

static void
adw_demo_page_tab_view_get_property (GObject    *object,
                                     guint       prop_id,
                                     GValue     *value,
                                     GParamSpec *pspec)
{
  AdwDemoPageTabView *self = ADW_DEMO_PAGE_TAB_VIEW (object);

  switch (prop_id) {
  case PROP_TITLE:
    g_value_set_string (value, self->title);
    break;
  case PROP_ICON:
    g_value_set_object (value, self->icon);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
adw_demo_page_tab_view_set_property (GObject      *object,
                                     guint         prop_id,
                                     const GValue *value,
                                     GParamSpec   *pspec)
{
  AdwDemoPageTabView *self = ADW_DEMO_PAGE_TAB_VIEW (object);

  switch (prop_id) {
  case PROP_TITLE:
    g_clear_pointer (&self->title, g_free);
    self->title = g_value_dup_string (value);
    break;
  case PROP_ICON:
    g_set_object (&self->icon, g_value_get_object (value));
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
}

static void
adw_demo_page_tab_view_class_init (AdwDemoPageTabViewClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = adw_demo_page_tab_view_finalize;
  object_class->get_property = adw_demo_page_tab_view_get_property;
  object_class->set_property = adw_demo_page_tab_view_set_property;

  props[PROP_TITLE] =
    g_param_spec_string ("title", NULL, NULL,
                         NULL,
                         G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  props[PROP_ICON] =
    g_param_spec_object ("icon", NULL, NULL,
                         G_TYPE_ICON,
                         G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, LAST_PROP, props);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Adwaita1/Demo/ui/pages/tab-view/adw-demo-page-tab-view.ui");

  gtk_widget_class_install_action (widget_class, "demo.run", NULL, (GtkWidgetActionActivateFunc) demo_run_cb);
}

static void
adw_demo_page_tab_view_init (AdwDemoPageTabView *self)
{
  self->icon = get_random_icon ();

  gtk_widget_init_template (GTK_WIDGET (self));
}


AdwDemoPageTabView *
adw_demo_page_tab_view_new (const char         *title)
{
  return g_object_new (ADW_TYPE_DEMO_PAGE_TAB_VIEW,
                       "title", title,
                       NULL);
}
